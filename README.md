vending-machine-kata
====================

This is a simple exercise vending-machine-kata - in which you will simulate the...
vending machine ( https://en.wikipedia.org/wiki/Vending_machine )

The project is maven based. There is provided maven wrapper so if do not have maven installed call `./mvnw` (or `mvnw.cmd` on Windows) to 
build it.
Below you will find requirements, key aspects and most importantly assignment itself.

Requirements
---------

* Solve the exercise using Java language. We prefer Java 7 or 8.

Key aspects
----------------

* Test-Driven Development
* object and domain design (Object-Oriented Programming, Domain-Driven Design)
* craftsmanship of production and test code (Clean Code)
* correctness of the algorithm
* usage of design patterns
* atomicity and readability of the commits

The assignment
------------

1. Vending machine contains products.
2. Products can be of different types (i.e. Cola drink 0.25l, chocolate bar, mineral water 0.33l and so on).
3. Products are on shelves.
4. One shelve can contain only one type of product (but multiple products).
5. Each product type has its own price.
6. Machine has a display.
7. If we select shelve number, display should show product price.
8. You can buy products by putting money into machine. Machine accepts denominations 5, 2, 1, 0.5, 0.2, 0.1.
9. After inserting a coin, display shows amount that must be added to cover product price.
10. After selecting a shelve and inserting enough money we will get the product and the change (but machine has to have money to be able to return the change).
11. After selecting a shelve and inserting insufficient money to buy a product, user has to press "Cancel" to get their money back.
12. If machine does not have enough money to give the change it must show a warning message and return the money user has put, and it should not give the product.
13. Machine can return change using only money that was put into it (or by someone at start or by people who bought goods). Machine cannot create it's own money!
