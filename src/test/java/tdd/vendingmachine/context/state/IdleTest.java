package tdd.vendingmachine.context.state;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.context.MachineContext;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.display.Display;
import tdd.vendingmachine.display.Message;
import tdd.vendingmachine.display.SimpleDisplay;
import tdd.vendingmachine.money.Coin;
import tdd.vendingmachine.money.CoinBank;
import tdd.vendingmachine.money.MainCoinBank;
import tdd.vendingmachine.product.Product;
import tdd.vendingmachine.product.ProductChamber;
import tdd.vendingmachine.product.ProductType;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Created by kropla on 18.01.17.
 */

@RunWith(MockitoJUnitRunner.class)
public class IdleTest {
    private static final String MSG_TEST_EXPECTED = "dummy text";
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private MachineState state;

    @Mock
    private VendingMachine machine;

    @Mock
    private Context context;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @Before
    public void initValues(){
        state = new Idle();
        doAnswer(invocationOnMock -> {
            System.out.println(MSG_TEST_EXPECTED);
            return null;
        }).when(machine).displayMessage(Message.MSG_WELCOME);
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

    @Test
    public void idleState_pushCancelButton_shouldCallDisplayMsg(){
        state.pushCancelButton(machine, context);

        String result = outContent.toString();
        assertTrue( result.contains(MSG_TEST_EXPECTED));
    }

    @Test
    public void idleState_initiatingState_shouldCallDisplayMsg(){
        state.initState(machine, context);

        String result = outContent.toString();
        assertTrue( result.contains(MSG_TEST_EXPECTED));
    }


    @Test
    public void idleState_coinHasBeenInserted_shouldGiveBackCoinToCoinDockOutShelf(){
        //G
        CoinBank usersCoinBank = new MainCoinBank();
        CoinBank coinDockOut = new MainCoinBank();
        CoinBank machineBank = mock(MainCoinBank.class);
        Display display = mock(SimpleDisplay.class);
        ProductChamber prodChamber = mock(ProductChamber.class);

        VendingMachine machine = spy(new VendingMachine(display,usersCoinBank, machineBank, coinDockOut, prodChamber));

        Coin coinToInput = Coin.FIVE;
        Map<Coin, Integer> expectedCoins = new EnumMap<>(Coin.class);
        expectedCoins.put(coinToInput, 1);

        //w
        state.inputCoin(machine, context, coinToInput);

        //t
        assertEquals(expectedCoins, coinDockOut.pull());
    }

    @Test
    public void idleState_twoCoinsHaveBeenInserted_shouldGiveBackAllCoinsToDockOutShelf(){
        //G
        CoinBank usersCoinBank = new MainCoinBank();
        CoinBank coinDockOut = new MainCoinBank();
        CoinBank machineBank = mock(MainCoinBank.class);
        Display display = mock(SimpleDisplay.class);
        ProductChamber prodChamber = mock(ProductChamber.class);

        VendingMachine machine = spy(new VendingMachine(display,usersCoinBank, machineBank, coinDockOut, prodChamber));

        Coin coinToInput = Coin.FIVE;
        Map<Coin, Integer> expectedCoins = new EnumMap<>(Coin.class);
        expectedCoins.put(coinToInput, 2);

        //w
        state.inputCoin(machine, context, coinToInput);
        state.inputCoin(machine, context, coinToInput);

        //t
        assertEquals(expectedCoins, coinDockOut.pull());
    }

    @Test
    public void idleState_treeDifferentCoinsHaveBeenInserted_shouldGiveBackAllCoinsToDockOutShelf(){
        //G
        CoinBank usersCoinBank = new MainCoinBank();
        CoinBank coinDockOut = new MainCoinBank();
        CoinBank machineBank = mock(MainCoinBank.class);
        Display display = mock(SimpleDisplay.class);
        ProductChamber prodChamber = mock(ProductChamber.class);

        VendingMachine machine = spy(new VendingMachine(display,usersCoinBank, machineBank, coinDockOut, prodChamber));

        Map<Coin, Integer> expectedCoins = new EnumMap<>(Coin.class);
        expectedCoins.put(Coin.FIVE, 1);
        expectedCoins.put(Coin.ONE_HALF, 1);
        expectedCoins.put(Coin.TWO_TENTHS, 1);

        //w
        state.inputCoin(machine, context, Coin.FIVE);
        state.inputCoin(machine, context, Coin.TWO_TENTHS);
        state.inputCoin(machine, context, Coin.ONE_HALF);


        //t
        assertEquals(expectedCoins, coinDockOut.pull());
    }

    @Test
    public void idleState_shelfNumberChosenWithExistingProduct_shouldChangedStateToWaitingForCoins(){
        //G
        int shelfNr = 1;
        CoinBank usersCoinBank =  mock(MainCoinBank.class);
        CoinBank coinDockOut =  mock(MainCoinBank.class);
        CoinBank machineBank = mock(MainCoinBank.class);
        Display display = mock(SimpleDisplay.class);
        ProductChamber prodChamber = mock(ProductChamber.class);

        VendingMachine machine = spy(new VendingMachine(display,usersCoinBank, machineBank, coinDockOut, prodChamber));

        Product productToReturn = new Product(shelfNr, new ProductType.Builder("Cola", 33.3f).build());
        when(machine.getChosenProduct()).thenReturn(productToReturn);
        when(usersCoinBank.getBalance()).thenReturn(BigDecimal.TEN);
        MachineContext ctx = new MachineContext(machine);
        //W
        ctx.setState(state);
        state.inputShelfNumber(machine, ctx, shelfNr);

        //T
        assertTrue(!(ctx.getState() instanceof Idle));
        assertTrue(ctx.getState() instanceof CoinWaiting);
    }

    @Test
    public void idleState_shelfNumberChosenWithNotExistingProduct_shouldRemainIdleState(){
        //G
        int shelfNr = 1;
        CoinBank usersCoinBank =  mock(MainCoinBank.class);
        CoinBank coinDockOut =  mock(MainCoinBank.class);
        CoinBank machineBank = mock(MainCoinBank.class);
        Display display = mock(SimpleDisplay.class);
        ProductChamber prodChamber = mock(ProductChamber.class);

        VendingMachine machine = spy(new VendingMachine(display,usersCoinBank, machineBank, coinDockOut, prodChamber));

        when(prodChamber.getProductInfoFromShelf(shelfNr)).thenReturn(null);
        //W
        state.inputShelfNumber(machine, context, shelfNr);
       // MachineState stateAfterChange = machine.getActualState();

        //T
        assertTrue(!(state instanceof CoinWaiting));
        assertTrue(state instanceof Idle);
    }

}
