package tdd.vendingmachine.context.state;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.context.MachineContext;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.money.Coin;
import tdd.vendingmachine.product.WrongProductTypeInsertIntoShelfException;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Created by kropla on 18.01.17.
 */

@RunWith(MockitoJUnitRunner.class)
public class CoinWaitingTest {
    private MachineState state;

    @Mock
    private VendingMachine machine;
    @Mock
    private Context context;

    @Before
    public void initValues(){
        state = new CoinWaiting();
    }

    @Test
    public void inputCoin_amountOfCoinValuesIsEnough_shouldCallChangeState() {

        MachineContext ctx = new MachineContext(machine);
        when(machine.isUserCoinAmountEnough()).thenReturn(true);

        state.inputCoin(machine, ctx, Coin.ONE_TENTH);

        assertTrue("State should ve of type CoinWaiting", !(ctx.getState() instanceof CoinWaiting));
        //should change to Idle cause: CoinWaiting -> CoinChanging -> ProdIssuing ->Idle
        assertTrue("State should changed to Idle", ctx.getState() instanceof Idle);
    }


    @Test
    public void pushCancelButton_shouldChangeStateToIdle() throws WrongProductTypeInsertIntoShelfException {
        MachineContext ctx = new MachineContext(machine);

        state.pushCancelButton(machine, ctx);

        assertTrue("State should changed to Idle", ctx.getState() instanceof Idle);
    }

}
