package tdd.vendingmachine.context.state;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.money.Coin;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

/**
 * Created by kropla on 24.01.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class CoinWaitingSimpleMsgTest {
    private static final String MSG_TEST_EXPECTED = "dummy text";
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private MachineState state;

    @Mock
    private VendingMachine machine;
    @Mock
    private Context context;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @Before
    public void initValues(){
        state = new CoinWaiting();

        doAnswer(invocationOnMock -> {
            System.out.println(MSG_TEST_EXPECTED);
            return null;
        }).when(machine).displayRemaingCoinAmount();
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

    @Test
    public void inputCoin_amountOfCoinValuesIsNotEnough_shouldRunDisplayRemainingAmount() {
        when(machine.isUserCoinAmountEnough()).thenReturn(false);

        state.inputCoin(machine, context, Coin.ONE_TENTH);
        String result = outContent.toString();
        assertTrue(result.contains(MSG_TEST_EXPECTED));

    }

    @Test
    public void inputShelfNr_shouldCallDisplayRemainCoinAmount() {
        state.inputShelfNumber(machine, context,1);
        String result = outContent.toString();
        assertTrue(result.contains(MSG_TEST_EXPECTED));
    }

    @Test
    public void initState_shouldCallDisplayRemainCoinAmount() {
        state.initState(machine, context);
        String result = outContent.toString();
        assertTrue(result.contains(MSG_TEST_EXPECTED));
    }
}
