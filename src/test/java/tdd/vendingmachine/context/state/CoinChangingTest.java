package tdd.vendingmachine.context.state;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.context.MachineContext;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.display.Display;
import tdd.vendingmachine.display.SimpleDisplay;
import tdd.vendingmachine.money.Coin;
import tdd.vendingmachine.money.CoinBank;
import tdd.vendingmachine.money.MainCoinBank;
import tdd.vendingmachine.money.NotSufficientCoinsInBankForChange;
import tdd.vendingmachine.product.*;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Created by kropla on 18.01.17.
 */

@RunWith(MockitoJUnitRunner.class)
public class CoinChangingTest {
    private static final String MSG_TEST_EXPECTED = "dummy text";
    private MachineState state;

    private VendingMachine machine;

    @Mock
    private Context context;

    @Before
    public void init(){
    }

    @Test
    public void initState_whenUserCoinsIsEqualToPrice_shouldChangeStateToIdle() {
        //G
        //return mock for user inputted coins
        Map<Coin, Integer> userInputtedCoins = new EnumMap<>(Coin.class);
        userInputtedCoins.put(Coin.FIVE, 3);
        userInputtedCoins.put(Coin.TWO_TENTHS, 2);
        userInputtedCoins.put(Coin.ONE_TENTH, 6);//16.0
        CoinBank userBank = new MainCoinBank();
        userBank.add(userInputtedCoins);

        CoinBank coinDockOut = new MainCoinBank();
        CoinBank machineBank = new MainCoinBank();
        Display display = mock(SimpleDisplay.class);
        ProductChamber prodChamber = mock(ProductChamber.class);

        machine = spy(new VendingMachine(
            display,
            userBank,
            machineBank,
            coinDockOut,
            prodChamber,
            mock(List.class)));

        //mock for chosen product
        Product productToReturn = new Product(1, new ProductType.Builder("Cola", 16.0f).build());
        when(machine.getChosenProduct()).thenReturn(productToReturn);

        state = new CoinChanging();
        MachineContext ctx = new MachineContext(machine);
        state.initState(machine, ctx);

        assertThat(ctx.getState(), notNullValue());
        assertThat(ctx.getState(), not(instanceOf (CoinChanging.class)));
        //state Idle cause: CoinChanging -> ProductIssueing -> Idle
        assertThat(ctx.getState(), instanceOf(Idle.class));
    }


    @Test
    public void initState_whenUserCoinsAmountIsBiggerThanPriceAndChangeCanBeGive_shouldReturnChange() {
        //G

        Map<Coin, Integer> userCoins = new EnumMap<>(Coin.class);
        userCoins.put(Coin.FIVE, 3);//15.0
        CoinBank userBank = new MainCoinBank();
        userBank.add(userCoins);

        CoinBank coinDockOut = new MainCoinBank();

        Map<Coin, Integer> machineCoins = new EnumMap<>(Coin.class);
        machineCoins.put(Coin.FIVE, 6);
        machineCoins.put(Coin.TWO, 6);
        machineCoins.put(Coin.ONE, 6);
        machineCoins.put(Coin.ONE_HALF, 6);
        machineCoins.put(Coin.TWO_TENTHS, 6);
        machineCoins.put(Coin.ONE_TENTH, 6);
        CoinBank machineBank = new MainCoinBank();
        machineBank.add(machineCoins);

        Display display = mock(SimpleDisplay.class);
        ProductChamber prodChamber = mock(ProductChamber.class);

        machine = spy(new VendingMachine(display, userBank, machineBank, coinDockOut, prodChamber));
        Product productToReturn = new Product(1, new ProductType.Builder("Cola", 13.9f).build());

        when(machine.getChosenProduct()).thenReturn(productToReturn);

        Map<Coin, Integer> expectCoinChange = new EnumMap<>(Coin.class);
        expectCoinChange.put(Coin.ONE,1);
        expectCoinChange.put(Coin.ONE_TENTH,1);

        //mock for chosen product
        //machine.setChosenProduct(productToReturn);

        state = new CoinChanging();

        //w
        state.initState(machine, context);

        //T
        assertEquals(expectCoinChange, coinDockOut.pull());
    }

    @Test
    public void initState_whenCoinsInBankIsNotEnough_shouldChangeStateToIdle() throws NotSufficientCoinsInBankForChange {
        //G

        VendingMachine machine = mock(VendingMachine.class);

        doThrow(new NotSufficientCoinsInBankForChange()).when(machine).changeCoins();

        state = new CoinChanging();
        MachineContext ctx = new MachineContext(machine);
        ctx.setState(state);
        //w
        ctx.initState();

        //T
        assertTrue(ctx.getState() instanceof Idle);
    }

    @Test
    public void coinInsert_shouldStayOnCoinChangingState() throws NotSufficientCoinsInBankForChange {
        //G

        VendingMachine machineMock = mock(VendingMachine.class);

        state = new CoinChanging();
        MachineContext ctx = new MachineContext(machineMock);
        ctx.setState(state);
        //w
        state.inputCoin(machineMock, ctx, Coin.FIVE);

        //T
        assertEquals(ctx.getState(), state);
        assertTrue(ctx.getState() instanceof CoinChanging);
    }

    @Test
    public void pushCancel_shouldStayOnCoinChangingState() throws NotSufficientCoinsInBankForChange {
        VendingMachine machine = mock(VendingMachine.class);

        state = new CoinChanging();
        MachineContext ctx = new MachineContext(machine);
        ctx.setState(state);
        //w
        state.pushCancelButton(machine, ctx);

        //T
        assertTrue(ctx.getState() instanceof CoinChanging);
    }

    @Test
    public void inputShelfNr_shouldStayOnCoinChangingState() throws NotSufficientCoinsInBankForChange {
        VendingMachine machine = mock(VendingMachine.class);

        state = new CoinChanging();
        MachineContext ctx = new MachineContext(machine);
        ctx.setState(state);
        //w
        state.inputShelfNumber(machine, ctx, 1);

        //T
        assertTrue(ctx.getState() instanceof CoinChanging);
    }
}
