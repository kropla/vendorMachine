package tdd.vendingmachine.context.state;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.context.MachineContext;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.display.SimpleDisplay;
import tdd.vendingmachine.money.Coin;
import tdd.vendingmachine.money.CoinBank;
import tdd.vendingmachine.money.MainCoinBank;
import tdd.vendingmachine.money.NotSufficientCoinsInBankForChange;
import tdd.vendingmachine.product.Product;
import tdd.vendingmachine.product.ProductChamber;
import tdd.vendingmachine.product.ProductType;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by kropla on 18.01.17.
 */

@RunWith(MockitoJUnitRunner.class)
public class ProductIssuingTest {
    private MachineState state;

    private VendingMachine machine;

    @Mock
    private Context context;

    @Test
    public void initState_shouldAddChosenProductToDeck() {
        //G

        ProductChamber prodChamber = new ProductChamber();
        List<Product> prodOutDeck = new ArrayList<>();

        machine = spy(new VendingMachine(
            mock(SimpleDisplay.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            prodChamber,
            prodOutDeck));

        //mock for chosen product
        Product productToReturn = new Product(1, new ProductType.Builder("Cola", 16.0f).build());
        when(machine.getChosenProduct()).thenReturn(productToReturn);

        List<Product> expected = new ArrayList<>();
        expected.add(productToReturn);

        state = new ProductIssuing();
        MachineContext ctx = new MachineContext(machine);
        state.initState(machine, ctx);

        assertEquals(expected, prodOutDeck);
    }

    @Test
    public void initState_afterAddProduct_shouldChangeStateToIdle() throws NotSufficientCoinsInBankForChange {
        //G
        machine = mock(VendingMachine.class);

        state = new ProductIssuing();
        MachineContext ctx = new MachineContext(machine);
        state.initState(machine, ctx);

        assertThat(ctx.getState(), notNullValue());
        assertThat(ctx.getState(), instanceOf(Idle.class));
    }

    @Test
    public void coinInsert_shouldStayOnProductIssuingState() throws NotSufficientCoinsInBankForChange {
        //G

        VendingMachine machineMock = mock(VendingMachine.class);

        state = new ProductIssuing();
        MachineContext ctx = new MachineContext(machineMock);
        ctx.setState(state);
        //w
        state.inputCoin(machineMock, ctx, Coin.FIVE);

        //T
        assertEquals(ctx.getState(), state);
        assertTrue(ctx.getState() instanceof ProductIssuing);
    }

    @Test
    public void coinInsert_shouldGiveBackInsertedCoin() throws NotSufficientCoinsInBankForChange {
        //G
        CoinBank coinOutDeck =  new MainCoinBank();

        machine = spy(new VendingMachine(
            mock(SimpleDisplay.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            coinOutDeck,
            mock(ProductChamber.class)));

        //mock for chosen product

        state = new ProductIssuing();
        MachineContext ctx = new MachineContext(machine);
        ctx.setState(state);
        Map<Coin,Integer> expectedCoins = new EnumMap<>(Coin.class);
        expectedCoins.put(Coin.FIVE, 1);

        //w
        state.inputCoin(machine, ctx, Coin.FIVE);

        //T
        assertEquals(expectedCoins, coinOutDeck.pull());
    }

    @Test
    public void pushCancel_shouldStayOnProductIssueState() throws NotSufficientCoinsInBankForChange {
        VendingMachine machine = mock(VendingMachine.class);

        state = new ProductIssuing();
        MachineContext ctx = new MachineContext(machine);
        ctx.setState(state);
        //w
        state.pushCancelButton(machine, ctx);

        //T
        assertTrue(ctx.getState() instanceof ProductIssuing);
    }

    @Test
    public void inputShelfNr_shouldStayOnProductIssueState() throws NotSufficientCoinsInBankForChange {
        VendingMachine machine = mock(VendingMachine.class);

        state = new ProductIssuing();
        MachineContext ctx = new MachineContext(machine);
        ctx.setState(state);
        //w
        state.inputShelfNumber(machine, ctx, 1);

        //T
        assertTrue(ctx.getState() instanceof ProductIssuing);
    }
}
