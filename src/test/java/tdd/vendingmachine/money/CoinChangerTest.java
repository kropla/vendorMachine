package tdd.vendingmachine.money;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by kropla on 17.01.17.
 */
public class CoinChangerTest {

    @Test
    public void whenInsertedMoreCoinsThanNeeded_shouldReturnProperChange() throws Exception {
        SimpleCoinChanger changer = new SimpleCoinChanger();
        Map<Coin, Integer> mainBankCoins = new EnumMap<>(Coin.class);
        mainBankCoins.put(Coin.FIVE, 5);
        mainBankCoins.put(Coin.TWO, 5);
        mainBankCoins.put(Coin.ONE, 5);
        mainBankCoins.put(Coin.ONE_HALF, 5);
        mainBankCoins.put(Coin.TWO_TENTHS, 5);
        mainBankCoins.put(Coin.ONE_TENTH, 5);


        Map<Coin, Integer> expectedChange= new EnumMap<>(Coin.class);
        expectedChange.put(Coin.ONE, 1);
        expectedChange.put(Coin.TWO_TENTHS, 1);
        expectedChange.put(Coin.ONE_TENTH, 1);


        CoinBank cBank = new MainCoinBank();
        cBank.add(mainBankCoins);
        BigDecimal price = new BigDecimal(13.70);
        BigDecimal insertedCoinsBalance = new BigDecimal(15.0);

        Map<Coin, Integer> resultCoinsMap = changer.change(price, insertedCoinsBalance, cBank);

        assertEquals(expectedChange, resultCoinsMap);

    }

    @Test
    public void shouldReturnException_whenThereIsNotEnoughCoinsToChange() throws Exception {
        SimpleCoinChanger changer = new SimpleCoinChanger();
        Map<Coin, Integer> mainBankCoins = new EnumMap<>(Coin.class);
        mainBankCoins.put(Coin.TWO, 5);
        mainBankCoins.put(Coin.ONE, 5);

        Map<Coin, Integer> expectedChange= new EnumMap<>(Coin.class);
        expectedChange.put(Coin.ONE, 1);
        expectedChange.put(Coin.TWO_TENTHS, 1);
        expectedChange.put(Coin.ONE_TENTH, 1);


        CoinBank cBank = new MainCoinBank();
        cBank.add(mainBankCoins);
        BigDecimal price = new BigDecimal(13.70);
        BigDecimal insertedCoinsBalance = new BigDecimal(15.0);

        try {
            Map<Coin, Integer> resultCoinsMap = changer.change(price, insertedCoinsBalance, cBank);
            fail("Expected an IndexOutOfBoundsException to be thrown");
        } catch(NotSufficientCoinsInBankForChange ex){
            assertThat(ex.getMessage(), is ("There is no sufficient coins in bank for change."));
            assertEquals(mainBankCoins, cBank.pull());
        }
    }
    @Test
    public void whenInsertedMoreCoinsThanNeeded_shouldReturnProperChange2() throws NotSufficientCoinsInBankForChange {

        Map<Coin, Integer> expectedChange = new EnumMap<>(Coin.class);
        expectedChange.put(Coin.FIVE, 1);
        expectedChange.put(Coin.TWO_TENTHS, 1);//

        Map<Coin, Integer> userCoins = new EnumMap<>(Coin.class);
        userCoins.put(Coin.FIVE, 3);
        userCoins.put(Coin.TWO, 1);
        userCoins.put(Coin.TWO_TENTHS, 5);//18.0
        CoinBank usersCoinBank = new MainCoinBank();
        usersCoinBank.add(userCoins);

        Map<Coin, Integer> bankCoins = new EnumMap<>(Coin.class);
        bankCoins.put(Coin.FIVE, 6);
        bankCoins.put(Coin.TWO, 6);
        bankCoins.put(Coin.ONE, 6);
        bankCoins.put(Coin.ONE_HALF, 6);
        bankCoins.put(Coin.ONE_TENTH, 6);
        bankCoins.put(Coin.TWO_TENTHS, 6);
        CoinBank mainCoinBank = new MainCoinBank();
        mainCoinBank.add(bankCoins);
        SimpleCoinChanger simpleCChanger =  new SimpleCoinChanger();


        assertEquals(expectedChange , simpleCChanger.change(
            BigDecimal.valueOf(12.8),
            usersCoinBank.getBalance(),
            mainCoinBank));
    }

    @Test
    public void whenInsertedMoreCoinsThanNeeded_shouldReturnProperChange3() throws NotSufficientCoinsInBankForChange {

        Map<Coin, Integer> expectedChange = new EnumMap<>(Coin.class);
        expectedChange.put(Coin.TWO, 2);

        Map<Coin, Integer> userCoins = new EnumMap<>(Coin.class);
        userCoins.put(Coin.FIVE, 2);
        userCoins.put(Coin.TWO, 1);
        CoinBank usersCoinBank = new MainCoinBank();
        usersCoinBank.add(userCoins);

        Map<Coin, Integer> bankCoins = new EnumMap<>(Coin.class);
        bankCoins.put(Coin.FIVE, 6);
        bankCoins.put(Coin.TWO, 6);
        bankCoins.put(Coin.ONE, 6);
        bankCoins.put(Coin.ONE_HALF, 6);
        bankCoins.put(Coin.ONE_TENTH, 6);
        bankCoins.put(Coin.TWO_TENTHS, 6);
        CoinBank mainCoinBank = new MainCoinBank();
        mainCoinBank.add(bankCoins);
        SimpleCoinChanger simpleCChanger =  new SimpleCoinChanger();


        assertEquals(expectedChange , simpleCChanger.change(
            BigDecimal.valueOf(8),
            usersCoinBank.getBalance(),
            mainCoinBank));
    }

    @Test
    public void shouldReturnDifferenceInIntTypeMUltiplyByTen(){
        SimpleCoinChanger simpleCChanger =  new SimpleCoinChanger();
        BigDecimal price = new BigDecimal(13.70);
        BigDecimal insertedCoinsBalance = new BigDecimal(15.0);

        int returnDiff = simpleCChanger.getDifferenceInIntWhitoutComma(insertedCoinsBalance, price);
        assertEquals(13, returnDiff);

    }

    @Test
    public void shouldReturnDifference(){
        SimpleCoinChanger simpleCChanger =  new SimpleCoinChanger();
        BigDecimal price = new BigDecimal(12.80);
        BigDecimal insertedCoinsBalance = new BigDecimal(18.0);

        int returnDiff = simpleCChanger.getDifferenceInIntWhitoutComma(insertedCoinsBalance, price);
        assertEquals(52, returnDiff);

    }
}
