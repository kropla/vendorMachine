package tdd.vendingmachine.money;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by kropla on 15.01.17.
 */
public class MainCoinBankTest {
    private Map<Coin, Integer> coins;
    private CoinBank coinBank;

    @Before
    public void init(){
        coinBank = new MainCoinBank();

        coins = new EnumMap<>(Coin.class);
        coins.put(Coin.FIVE, 1);//5
        coins.put(Coin.TWO, 2);//9
        coins.put(Coin.ONE_HALF, 1);//9.50
        coins.put(Coin.TWO_TENTHS, 1);//9.70
        coins.put(Coin.ONE_TENTH, 2);//9.90
    }

    @Test
    public void addCoin_whenInsertingSingleCoinTheSameType_shouldAddCoinsToBank(){

        coinBank.add(Coin.FIVE);
        coinBank.add(Coin.FIVE);
        coinBank.add(Coin.FIVE);

        assertEquals(3, coinBank.getCoinsQtm());
        assertEquals(BigDecimal.valueOf(15.0), coinBank.getBalance());
    }

    @Test
    public void addCoin_whenInsertingDIfferentCoins_shouldAddDifferentCoinsToBank(){

        coinBank.add(Coin.FIVE);
        coinBank.add(Coin.TWO);
        coinBank.add(Coin.ONE);
        coinBank.add(Coin.ONE_HALF);
        coinBank.add(Coin.TWO_TENTHS);
        coinBank.add(Coin.ONE_TENTH);

        assertEquals(6, coinBank.getCoinsQtm());
        assertEquals(BigDecimal.valueOf(8.8), coinBank.getBalance());
    }


    @Test
    public void addCoinsGroup_whenAddingCoinsToEmptyBank_shouldAddAllCollectionOfCoinsToBank(){
        //G on init method
        //W
        coinBank.add(coins);

        assertEquals(7, coinBank.getCoinsQtm());
        assertEquals(BigDecimal.valueOf(9.9), coinBank.getBalance());
    }

    @Test
    public void addCoinsGroup_whenAddingCoinsToNotEmptyBank_shouldAddAllCollectionOfCoinsToBank(){
        //G on init method
        //W
        coinBank.add(coins);
        coinBank.add(coins);

        //T

        assertEquals(14, coinBank.getCoinsQtm());
        assertEquals(BigDecimal.valueOf(19.8), coinBank.getBalance());
    }

    @Test
    public void addCoinsGroup_whenAddingSingleCoinsGroup_shouldAddAllCollectionOfCoinsToBank(){
        //G on init method
        Map<Coin, Integer> coins = new EnumMap<>(Coin.class);
        coins.put(Coin.FIVE, 1);
        Map<Coin, Integer> coins2 = new EnumMap<>(Coin.class);
        coins2.put(Coin.TWO, 1);
        Map<Coin, Integer> coins3 = new EnumMap<>(Coin.class);
        coins3.put(Coin.TWO_TENTHS, 1);
        //W

        coinBank.add(coins);
        coinBank.add(coins2);
        coinBank.add(coins3);

        //T

        assertEquals(3, coinBank.getCoinsQtm());
        assertEquals(BigDecimal.valueOf(7.2), coinBank.getBalance());
    }

    @Test
    public void pullAllCoins_whenRemovingWholeBankCoins_coinsBankShouldBeEmpty(){
        //G on init method
        //W
        coinBank.add(coins);

        Map<Coin,Integer> resultMap = coinBank.pull();

        assertEquals( BigDecimal.valueOf(0.0) , coinBank.getBalance());
        assertEquals(0, coinBank.getCoinsQtm());
        assertEquals(coins, resultMap);

    }
    @Test
    public void pullCoin_whenRemovingSpecificCoinType_coinsBankShouldBeUpdated_andReturnProperQtm(){

        coinBank.add(coins);

        Map<Coin,Integer> resultMap = coinBank.pull(Coin.TWO);

        assertEquals( BigDecimal.valueOf(5.9) , coinBank.getBalance());
        assertEquals( 5, coinBank.getCoinsQtm());
        assertTrue( resultMap.get(Coin.TWO) == 2);

    }

    @Test
    public void pullCoin_whenRemovingSpecificNotExistingCoinType_shouldReturnZero(){

        coinBank.add(coins);

        Map<Coin,Integer> resultMap = coinBank.pull(Coin.ONE);

        assertEquals( BigDecimal.valueOf(9.9) , coinBank.getBalance());
        assertEquals( 7, coinBank.getCoinsQtm());
        assertTrue( resultMap.get(Coin.ONE) == 0);

    }

    @Test
    public void pullCoinWithQuantity_whenRemovingSpecificQtmOfCoinType_shouldUpdateBank(){

        coinBank.add(coins);

        Map<Coin, Integer> resultMap = coinBank.pull(Coin.TWO, 1);

        assertEquals( BigDecimal.valueOf(7.9) , coinBank.getBalance());
        assertEquals( 6, coinBank.getCoinsQtm());
        assertTrue( resultMap.get(Coin.TWO) == 1);

    }


    @Test
    public void pullCoinWithQuantity_whenRemovingMoreCoinsThatAreInBank_shouldResultWithOnlyAvailableQtm(){

        coinBank.add(coins);

        Map<Coin, Integer> resultMap = coinBank.pull(Coin.TWO, 5);

        assertEquals( BigDecimal.valueOf(5.9), coinBank.getBalance());
        assertEquals( 5, coinBank.getCoinsQtm());
        assertEquals( 2, (int)resultMap.get(Coin.TWO));

    }



    @Test
    public void pullCoinWithQuantity_whenRemovingTheSameQtmCoinsThatAreInBank_shouldResultWithOnlyAvailableQtm(){

        coinBank.add(coins);

        Map<Coin, Integer> resultMap = coinBank.pull(Coin.TWO, 2);

        assertEquals( BigDecimal.valueOf(5.9) , coinBank.getBalance());
        assertEquals( 5, coinBank.getCoinsQtm());
        assertEquals( 2, (int)resultMap.get(Coin.TWO));

    }

    @Test
    public void pullCoinWithQuantity_whenRemovingNotExistingCoins_shouldResultWithZero(){

        coinBank.add(coins);

        Map<Coin, Integer> resultMap = coinBank.pull(Coin.ONE, 2);

        assertEquals( BigDecimal.valueOf(9.9) , coinBank.getBalance());
        assertEquals( 7, coinBank.getCoinsQtm());
        assertEquals( 0, (int)resultMap.get(Coin.ONE));

    }
}
