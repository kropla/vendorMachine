package tdd.vendingmachine.display;

import org.junit.Test;
import uk.org.lidalia.slf4jtest.TestLogger;
import uk.org.lidalia.slf4jtest.TestLoggerFactory;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static uk.org.lidalia.slf4jtest.LoggingEvent.info;

/**
 * Created by kropla on 21.01.17.
 */
public class LoggerOutTest {

    private Displayer loggerDisplayer;
    TestLogger logger = TestLoggerFactory.getTestLogger(LoggerDisplayer.class);

    @Test
    public void logger_shouldDisplayMsgWithLogger(){
        loggerDisplayer = new LoggerDisplayer();

        loggerDisplayer.show("test");
        assertThat(logger.getLoggingEvents(), is(asList(info("test"))));
    }
}
