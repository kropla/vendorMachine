package tdd.vendingmachine.display;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;
import static tdd.vendingmachine.display.Message.*;

/**
 * Created by kropla on 14.01.17.
 */
public class SystemOutDisplayTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private Display sysOutDisplay;
    private ResourceBundle resourceBundle ;


    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        resourceBundle = ResourceBundle.getBundle("displayMessages", Locale.ENGLISH);
        sysOutDisplay = new SimpleDisplay(resourceBundle, new SystemOutDisplayer());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }



    @Test
    public void shouldDisplay_WelcomeMessage_inEnglishLang_WithNewLine(){

        String expected = resourceBundle.getString(MSG_WELCOME.getPropertyName());

        sysOutDisplay.showMessage(MSG_WELCOME);
        String result = removeLFAndCRLF(outContent.toString());
        assertEquals(expected, result);
    }

    @Test
    public void shouldDisplay_ProductPrice_inEnglishLang_withNewLine(){
        String expected = "22.0";
        Double priceIn = 22.0;

        sysOutDisplay.showProductPrice(priceIn);

        String result = removeLFAndCRLF(outContent.toString());
        assertEquals(expected, result);
    }

    @Test
    public void shouldDisplay_ChangeWarningMsg_inEnglishLang_withNewLine(){
        String expected = resourceBundle.getString(MSG_CHANGE_WARNING.getPropertyName());

        sysOutDisplay.showMessage(MSG_CHANGE_WARNING);

        String result = removeLFAndCRLF(outContent.toString());
        assertEquals(expected, result);
    }

    @Test
    public void shouldDisplay_CancelMsg_inEnglishLang_withNewLine(){
        String expected = resourceBundle.getString(MSG_CANCEL.getPropertyName());

        sysOutDisplay.showMessage(MSG_CANCEL);

        String result = removeLFAndCRLF(outContent.toString());
        assertEquals(expected, result);
    }

    @Test
    public void shouldDisplay_wrongShelfMsg_inEnglishLang_withNewLine(){
        String expected = resourceBundle.getString(MSG_WRONG_SHELF.getPropertyName());

        sysOutDisplay.showMessage(MSG_WRONG_SHELF);

        String result = removeLFAndCRLF(outContent.toString());
        assertEquals(expected, result);
    }

    /**
     * Because of the systems differences in showing lineFeeds (linux lf, windows CRLF) last sign should be removed.
     * @param s String in
     * @return String without CRLF
     */
    private String removeLFAndCRLF(String s) {
        StringBuilder out = new StringBuilder(s.substring(0, s.length()-2));
        char[] workArray = s.substring(s.length()-2).toCharArray();
        for(char c : workArray){
            if(c != 10 && c != 13){
                out.append(c);
            }
        }

        return out.toString();
    }
}
