package tdd.vendingmachine.product;

import org.junit.Test;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by kropla on 15.01.17.
 */
public class ProductTypeTest {
    @Test
    public void shouldReturnNotEquals_whenTestingWithDifferentNames(){

        ProductType prodType1 = new ProductType.Builder("test1", 0).build();
        ProductType prodType2 = new ProductType.Builder("test2", 0).build();

        assertNotEquals(prodType1, prodType2);
    }

    @Test
    public void shouldReturnNotEquals_whenTestingWithDifferentVendorNames(){

        ProductType prodType1 = new ProductType.Builder("test1", 0)
            .weightInGrams(0).capacityInLiters(0f).heightInMilimeters(0).widthInMilimeters(0).vendorName("test2")
            .build();
        ProductType prodType2 = new ProductType.Builder("test1", 0)
            .weightInGrams(0).capacityInLiters(0f).heightInMilimeters(0).widthInMilimeters(0).vendorName("test1")
            .build();

        assertNotEquals(prodType1, prodType2);
    }

}
