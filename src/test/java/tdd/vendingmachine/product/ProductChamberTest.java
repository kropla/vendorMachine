package tdd.vendingmachine.product;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by kropla on 14.01.17.
 */
public class ProductChamberTest {
    private static final Logger LOG = LoggerFactory.getLogger(ProductChamberTest.class);
    private ProductChamber productChamber;
    private ProductType prodTypeColaLight;
    private ProductType prodTypeBar;

    @Before
    public void  initChamber(){
        productChamber = new ProductChamber();
        prodTypeColaLight = new ProductType.Builder("Cola light 0,25l", 4.50f).capacityInLiters(0.25f)
            .heightInMilimeters(110).vendorName("CocaCola").widthInMilimeters(55)
            .build();

        prodTypeBar = new ProductType.Builder("Snickers", 2f).weightInGrams(80)
            .heightInMilimeters(20).vendorName("Mars").weightInGrams(80).widthInMilimeters(100)
            .build();
    }


    @Test
    public void shouldAddOneProduct_ifChamberIsEmpty() throws Exception{

        Product product = new Product(12, prodTypeColaLight);
        int shelfNumber = 1;

        productChamber.add(shelfNumber, product);

        assertEquals("On shelf 1 should be one product", 1, getProductChamberQuantityOnShelf(1, productChamber));
    }

    @Test
    public void shouldAddTwoProductsToTheSameShelf() throws Exception{
        Product product = new Product(12, prodTypeColaLight);
        Product product2 = new Product(11,prodTypeColaLight);

        int shelfNumber = 1;

        productChamber.add(shelfNumber, product);
        productChamber.add(shelfNumber, product2);


        assertEquals("On shelf 1 should be two products",2, getProductChamberQuantityOnShelf(1, productChamber));
    }

    @Test
    public void whenAddingOtherTypeOfProductToTheSameShelf_shouldRaiseError() throws Exception{

        Product product = new Product(12, prodTypeColaLight);
        Product product2 = new Product(11, prodTypeBar);
        int shelfNumber = 1;

        productChamber.add(shelfNumber, product);

        try {
            productChamber.add(shelfNumber, product2);
            fail("Expected an IndexOutOfBoundsException to be thrown");
        } catch(WrongProductTypeInsertIntoShelfException ex){
            assertThat(ex.getMessage(), is ("On the single shelf there must be insterted products of the same types"));
            assertEquals("On shelf 1 should be one product", 1, getProductChamberQuantityOnShelf(1, productChamber));
        }
    }

    @Test
    public void shouldReturnNullProductInfo_FromEmptyShelf(){
        int shelfNumber = 1;

        Product productFromShelf = productChamber.getProductInfoFromShelf(shelfNumber);

        assertNull(productFromShelf);

    }

    @Test
    public void shouldReturnProductInfo_FromNotEmptyShelf() throws WrongProductTypeInsertIntoShelfException {
        int shelfNumber = 1;

        Product product = new Product(12, prodTypeColaLight);
        Product product2 = new Product(11, prodTypeBar);
        productChamber.add(shelfNumber,product);
        productChamber.add(shelfNumber,product);
        productChamber.add(shelfNumber+1,product2);
        productChamber.add(shelfNumber+1,product2);

        Product productFromShelf = productChamber.getProductInfoFromShelf(shelfNumber);

        assertNotNull(productFromShelf);

    }

    @Test
    public void shouldRemoveOneProductFromShelf() throws WrongProductTypeInsertIntoShelfException {
        int shelfNumber = 1;

        Product productToRemove = new Product(12, prodTypeColaLight);
        Product product2 = new Product(11, prodTypeBar);
        Product product3 = new Product(13, prodTypeBar);
        productChamber.add(shelfNumber,productToRemove);
        productChamber.add(shelfNumber+1,product2);
        productChamber.add(shelfNumber+1,product3);

        assertEquals("On shelf 1 should be one product", 1, getProductChamberQuantityOnShelf(1, productChamber));

        productChamber.removeOneProductFromShelf(shelfNumber);

        assertEquals("after removing product, shelf 1 should be empty",0, getProductChamberQuantityOnShelf(1, productChamber));

    }

    public static void assertProductChamberIsFull(ProductChamber chamber) {
        assertTrue(chamber.getProductInfoFromShelf(1) != null);
        assertTrue(chamber.getProductInfoFromShelf(2) != null);
        assertTrue(chamber.getProductInfoFromShelf(3) != null);
        assertTrue(chamber.getProductInfoFromShelf(4) != null);
    }

    public static int getProductChamberQuantityOnShelf(int shelfNr, ProductChamber chamber) {
        int count = 0;
        List<Product> productsToAddOnShelf = new ArrayList<>();
        Product prodToRemove = chamber.getProductInfoFromShelf(shelfNr);
        while (prodToRemove != null){
            productsToAddOnShelf.add(prodToRemove);
            count++;
            chamber.removeOneProductFromShelf(shelfNr);
            prodToRemove = chamber.getProductInfoFromShelf(shelfNr);
        }

        for (Product prod : productsToAddOnShelf){
            try {
                chamber.add(shelfNr, prod);
            } catch (WrongProductTypeInsertIntoShelfException e) {
                LOG.error("", e);
            }
        }
        return count;
    }


}
