package tdd.vendingmachine.machine;

import org.junit.Test;
import tdd.vendingmachine.display.Display;
import tdd.vendingmachine.money.Coin;
import tdd.vendingmachine.money.CoinBank;
import tdd.vendingmachine.money.MainCoinBank;
import tdd.vendingmachine.money.NotSufficientCoinsInBankForChange;
import tdd.vendingmachine.product.*;

import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class VendingMachineTest {

    @Test
    public void display_shouldCallDisplaySowProductPrice(){

        Map< Coin, Integer> expectedCoins = new EnumMap<>(Coin.class);
        expectedCoins.put(Coin.FIVE, 2);
        expectedCoins.put(Coin.TWO, 1);
        expectedCoins.put(Coin.TWO_TENTHS, 4);//12.8
        Display display = mock(Display.class);

        CoinBank usersCoinBank = new MainCoinBank();
        usersCoinBank.add(expectedCoins);

        VendingMachine machine = spy(new VendingMachine(
            display,
            usersCoinBank,
            mock(CoinBank.class),
            mock(CoinBank.class),
            mock(ProductChamber.class)
        ));
        Product productToReturn = new Product(1, new ProductType.Builder("Cola", BigDecimal.valueOf(33.3)).build());

        doAnswer(invocationOnMock -> {
            double remainPrice = (double) invocationOnMock.getArguments()[0];
            assertEquals(BigDecimal.valueOf(20.5), BigDecimal.valueOf(remainPrice));
            return null;
        }).when(display).showProductPrice(anyDouble());

        when(machine.getChosenProduct()).thenReturn(productToReturn);

        machine.displayRemaingCoinAmount();
    }

    @Test
    public void userCoinAmount_whenUserCoinsAreEqualToPrice_shouldReturnTrue(){

        Map< Coin, Integer> expectedCoins = new EnumMap<>(Coin.class);
        expectedCoins.put(Coin.FIVE, 2);
        expectedCoins.put(Coin.TWO, 1);
        expectedCoins.put(Coin.TWO_TENTHS, 4);//12.8

        CoinBank usersCoinBank = new MainCoinBank();
        usersCoinBank.add(expectedCoins);

        VendingMachine machine = spy(new VendingMachine(
            mock(Display.class),
            usersCoinBank,
            mock(CoinBank.class),
            mock(CoinBank.class),
            mock(ProductChamber.class)
        ));
        Product productToReturn = new Product(1,
            new ProductType.Builder("Cola",
                BigDecimal.valueOf(12.8)).build());

        when(machine.getChosenProduct()).thenReturn(productToReturn);

        boolean check = machine.isUserCoinAmountEnough();
        assertTrue(check);
    }
    @Test
    public void userCoinAmount_whenUserCoinsAreBiggerToPrice_shouldReturnFalse(){

        Map< Coin, Integer> expectedCoins = new EnumMap<>(Coin.class);
        expectedCoins.put(Coin.FIVE, 2);
        expectedCoins.put(Coin.TWO, 1);
        expectedCoins.put(Coin.TWO_TENTHS, 5);//12.2

        CoinBank usersCoinBank = new MainCoinBank();
        usersCoinBank.add(expectedCoins);

        VendingMachine machine = spy(new VendingMachine(
            mock(Display.class),
            usersCoinBank,
            mock(CoinBank.class),
            mock(CoinBank.class),
            mock(ProductChamber.class)
        ));
        Product productToReturn = new Product(1,
            new ProductType.Builder("Cola",
                BigDecimal.valueOf(12.8)).build());

        when(machine.getChosenProduct()).thenReturn(productToReturn);

        boolean check = machine.isUserCoinAmountEnough();
        assertTrue(check);
    }

    @Test
    public void createOrder_shouldSetChosenProduct() throws WrongProductTypeInsertIntoShelfException{
        Product productToReturn = new Product(1,
            new ProductType.Builder("Cola",
                BigDecimal.valueOf(12.8)).build());

        ProductChamber prodChamber = mock(ProductChamber.class);


        VendingMachine machine = spy(new VendingMachine(
            mock(Display.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            prodChamber
        ));
        when(prodChamber.getProductInfoFromShelf(anyInt())).thenReturn(productToReturn);

        machine.createOrder(1);

        assertEquals(Integer.valueOf(1), machine.getChosenShelfNr());
        assertEquals(productToReturn, machine.getChosenProduct());

    }

    @Test
    public void createOrder_shouldRemoveChosenProductFromChamber() throws WrongProductTypeInsertIntoShelfException{
        Product productToReturn = new Product(1,
            new ProductType.Builder("Cola",
                BigDecimal.valueOf(12.8)).build());

        ProductChamber prodChamber = new ProductChamber();
        prodChamber.add(1,productToReturn);

        VendingMachine machine = spy(new VendingMachine(
            mock(Display.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            prodChamber
        ));
        //when(prodChamber.getProductInfoFromShelf(anyInt())).thenReturn(productToReturn);

        ProductChamber expected = new ProductChamber();

        machine.createOrder(1);
        //assertTrue(ProductChamberTest.isChamberEmpty(prodChamber));
    }

    @Test
    public void returnProdToChamber() {
        Product productToReturn = new Product(1,
            new ProductType.Builder("Cola",
                BigDecimal.valueOf(12.8)).build());
        ProductChamber prodChamber = new ProductChamber();


        VendingMachine machine = spy(new VendingMachine(
            mock(Display.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            prodChamber
        ));
        when(machine.getChosenProduct()).thenReturn(productToReturn);
        when(machine.getChosenShelfNr()).thenReturn(1);

        machine.returnProductToChamber();

        assertEquals(productToReturn, prodChamber.getProductInfoFromShelf(1));

    }

    @Test
    public void changeCoins() throws NotSufficientCoinsInBankForChange {
        Product productToReturn = new Product(1,
            new ProductType.Builder("Cola",
                BigDecimal.valueOf(12.8)).build());

        Map< Coin, Integer> expectedChange= new EnumMap<>(Coin.class);
        expectedChange.put(Coin.FIVE, 1);
        expectedChange.put(Coin.TWO_TENTHS, 1);//

        Map< Coin, Integer> userCoins = new EnumMap<>(Coin.class);
        userCoins.put(Coin.FIVE, 3);
        userCoins.put(Coin.TWO, 1);
        userCoins.put(Coin.TWO_TENTHS, 5);//18.0
        CoinBank usersCoinBank = new MainCoinBank();
        usersCoinBank.add(userCoins);

        Map< Coin, Integer> bankCoins = new EnumMap<>(Coin.class);
        bankCoins.put(Coin.FIVE, 6);
        bankCoins.put(Coin.TWO, 6);
        bankCoins.put(Coin.ONE, 6);
        bankCoins.put(Coin.ONE_HALF, 6);
        bankCoins.put(Coin.ONE_TENTH, 6);
        bankCoins.put(Coin.TWO_TENTHS, 6);
        CoinBank mainCoinBank = new MainCoinBank();
        mainCoinBank.add(bankCoins);

        CoinBank coinOutDock = new MainCoinBank();

        VendingMachine machine = spy(new VendingMachine(
            mock(Display.class),
            usersCoinBank,
            mainCoinBank,
            coinOutDock,
            mock(ProductChamber.class)
        ));
        when(machine.getChosenProduct()).thenReturn(productToReturn);
        when(machine.getChosenShelfNr()).thenReturn(1);

        machine.changeCoins();

        assertTrue(!coinOutDock.pull().isEmpty());

    }

    @Test
    public void addProduct_whenChamberIsEmpty() throws WrongProductTypeInsertIntoShelfException {
        Product productToReturn = new Product(1,
            new ProductType.Builder("Cola",
                BigDecimal.valueOf(12.8)).build());
        ProductChamber prodChamber = new ProductChamber();


        VendingMachine machine = spy(new VendingMachine(
            mock(Display.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            prodChamber
        ));

        when(machine.getChosenShelfNr()).thenReturn(1);
        int shelfNr = 1;
        machine.addProduct(shelfNr, productToReturn);

        assertEquals(1, ProductChamberTest.getProductChamberQuantityOnShelf(1,prodChamber));
        assertEquals(productToReturn, prodChamber.getProductInfoFromShelf(1));
    }

    @Test
    public void addProducts_whenChamberIsEmpty() throws WrongProductTypeInsertIntoShelfException {
        Product product1 = new Product(1,
            new ProductType.Builder("Cola",
                BigDecimal.valueOf(12.8)).build());
        Product product2 = new Product(2,
            new ProductType.Builder("Cola Light",
                BigDecimal.valueOf(12.8)).build());
        ProductChamber prodChamber = new ProductChamber();


        VendingMachine machine = spy(new VendingMachine(
            mock(Display.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            mock(CoinBank.class),
            prodChamber
        ));

        when(machine.getChosenShelfNr()).thenReturn(1);
        int shelfNr = 0;
        machine.addProduct(++shelfNr, product1);
        machine.addProduct(++shelfNr, product2);

        assertEquals(1, ProductChamberTest.getProductChamberQuantityOnShelf(1,prodChamber));
        assertEquals(product1, prodChamber.getProductInfoFromShelf(1));
        assertEquals(product2, prodChamber.getProductInfoFromShelf(2));
    }
}
