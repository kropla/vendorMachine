package tdd.vendingmachine.runner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.context.MachineContext;
import tdd.vendingmachine.context.state.Idle;
import tdd.vendingmachine.display.Display;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.money.Coin;
import tdd.vendingmachine.money.CoinBank;
import tdd.vendingmachine.money.MainCoinBank;
import tdd.vendingmachine.product.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

/**
 * Created by kropla on 26.01.17.
 */

@RunWith(MockitoJUnitRunner.class)
public class EmptyProductChamberTest {
    private VendingMachine machine;
    private Context ctx;
    private CoinBank userCoinBank = new MainCoinBank();
    private CoinBank machineBank = new MainCoinBank();
    private CoinBank coinOutDeck = new MainCoinBank();
    private ProductChamber productChamber = new ProductChamber();
    private List<Product> productOutDeck;


    @Before
    public void initMachine(){
        createMachine();
        createContext();
    }


    private void createMachine() {
        Display display  = mock(Display.class);
        userCoinBank = new MainCoinBank();
        machineBank = new MainCoinBank();
        coinOutDeck = new MainCoinBank();
        productChamber = new ProductChamber();
        productOutDeck =  new ArrayList<>();

        machine = new VendingMachine(
            display,
            userCoinBank,
            machineBank,
            coinOutDeck,
            productChamber,
            productOutDeck
        );
    }

    private void createContext() {
        ctx = new MachineContext(machine);
        ctx.setState(new Idle());
    }

    @Test
    public void insertCoin_whenIdleStateWithoutSelectedProduct_shouldGiveBackCoin(){
        CoinBank expectedCoinOutDeck = new MainCoinBank();
        expectedCoinOutDeck.add(Coin.ONE);

        ctx.inputCoin(Coin.ONE);

        assertEquals(new MainCoinBank().getCoinsQtm(), userCoinBank.getCoinsQtm());
        assertEquals(new MainCoinBank().getCoinsQtm(), machineBank.getCoinsQtm());
        //assertEquals(new ProductChamber().isEmpty(), productChamber.isEmpty());
        assertEquals(new ArrayList<Product>(), productOutDeck);
        assertEquals(expectedCoinOutDeck.getCoinsQtm(), coinOutDeck.getCoinsQtm());
        assertEquals(expectedCoinOutDeck.getBalance(), coinOutDeck.getBalance());
        assertEquals(expectedCoinOutDeck.pull(), coinOutDeck.pull());

    }

    @Test
    public void pushCancel_whenIdleStateWithoutSelectedProduct_shoulChangeNothing(){

        ctx.pushCancelButton();

        assertEquals(new MainCoinBank().getCoinsQtm(), userCoinBank.getCoinsQtm());
        assertEquals(new MainCoinBank().getCoinsQtm(), machineBank.getCoinsQtm());
        //assertEquals(new ProductChamber().isEmpty(), productChamber.isEmpty());
        assertEquals(new ArrayList<Product>(), productOutDeck);
        assertEquals(new MainCoinBank().getCoinsQtm(), coinOutDeck.getCoinsQtm());
        assertEquals(new MainCoinBank().getBalance(), coinOutDeck.getBalance());
        assertEquals(new MainCoinBank().pull(), coinOutDeck.pull());

    }

    @Test
    public void shelfChosen_whenIdleStateAndEmptyProductChamber_shouldChangeNothing(){

        ctx.inputShelfNumber(1);

        assertEquals(new MainCoinBank().getCoinsQtm(), userCoinBank.getCoinsQtm());
        assertEquals(new MainCoinBank().getCoinsQtm(), machineBank.getCoinsQtm());
        //assertEquals(new ProductChamber().isEmpty(), productChamber.isEmpty());
        assertEquals(new ArrayList<Product>(), productOutDeck);
        assertEquals(new MainCoinBank().getCoinsQtm(), coinOutDeck.getCoinsQtm());
        assertEquals(new MainCoinBank().getBalance(), coinOutDeck.getBalance());
        assertEquals(new MainCoinBank().pull(), coinOutDeck.pull());

    }


    @Test
    public void addProduct_whenIdleState_shouldAddProdToChamber() throws WrongProductTypeInsertIntoShelfException {
        int shelfNr = 0;
        int prodId = 0;

        Product product1 = new Product(++prodId, new ProductType.Builder("Cola", BigDecimal.valueOf(33.3)).vendorName("CocaCola").build());
        Product product2 = new Product(++prodId, new ProductType.Builder("Cola Light", BigDecimal.valueOf(21.0)).vendorName("CocaCola").build());
        Product product3 = new Product(++prodId, new ProductType.Builder("Snickers Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());


        ProductChamber expectedChamber = new ProductChamber();
        expectedChamber.add(++shelfNr, product1);
        expectedChamber.add(++shelfNr, product2);
        expectedChamber.add(++shelfNr, product3);

        insertProducts();

        assertEquals(new MainCoinBank().getCoinsQtm(), userCoinBank.getCoinsQtm());
        assertEquals(new MainCoinBank().getCoinsQtm(), machineBank.getCoinsQtm());
        assertEquals(new MainCoinBank().getCoinsQtm(), coinOutDeck.getCoinsQtm());
        assertEquals(new ArrayList<Product>(), productOutDeck);
     //   assertEquals(expectedChamber.isEmpty(), productChamber.isEmpty());
        assertEquals(expectedChamber.getProductInfoFromShelf(1), productChamber.getProductInfoFromShelf(1));
        assertEquals(expectedChamber.getProductInfoFromShelf(2), productChamber.getProductInfoFromShelf(2));
        assertEquals(expectedChamber.getProductInfoFromShelf(3), productChamber.getProductInfoFromShelf(3));
        assertEquals(ProductChamberTest.getProductChamberQuantityOnShelf(1,expectedChamber), ProductChamberTest.getProductChamberQuantityOnShelf(1, productChamber));
        assertEquals(ProductChamberTest.getProductChamberQuantityOnShelf(2,expectedChamber), ProductChamberTest.getProductChamberQuantityOnShelf(2, productChamber));
        assertEquals(ProductChamberTest.getProductChamberQuantityOnShelf(3,expectedChamber), ProductChamberTest.getProductChamberQuantityOnShelf(3, productChamber));
    }


    private void insertProducts() throws WrongProductTypeInsertIntoShelfException {
        int shelfNr = 0;
        int prodId = 0;

        Product product1 = new Product(++prodId, new ProductType.Builder("Cola", BigDecimal.valueOf(33.3)).vendorName("CocaCola").build());
        Product product2 = new Product(++prodId, new ProductType.Builder("Cola Light", BigDecimal.valueOf(21.0)).vendorName("CocaCola").build());
        Product product3 = new Product(++prodId, new ProductType.Builder("Snickers Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());

        machine.addProduct(++shelfNr, product1);
        machine.addProduct(++shelfNr, product2);
        machine.addProduct(++shelfNr, product3);
    }


}
