package tdd.vendingmachine.runner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.context.MachineContext;
import tdd.vendingmachine.context.state.Idle;
import tdd.vendingmachine.display.Display;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.money.Coin;
import tdd.vendingmachine.money.CoinBank;
import tdd.vendingmachine.money.MainCoinBank;
import tdd.vendingmachine.product.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

/**
 * Created by kropla on 26.01.17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NotEmptyProductChamberTest {
    private VendingMachine machine;
    private Context ctx;
    private CoinBank userCoinBank = new MainCoinBank();
    private CoinBank machineBank = new MainCoinBank();
    private CoinBank coinOutDeck = new MainCoinBank();
    private ProductChamber productChamber = new ProductChamber();
    private List<Product> productOutDeck;


    @Before
    public void initMachine() throws WrongProductTypeInsertIntoShelfException {
        createMachine();
        createContext();
        insertProducts();
    }


    private void createMachine() {
        Display display  = mock(Display.class);
        userCoinBank = new MainCoinBank();
        machineBank = new MainCoinBank();
        coinOutDeck = new MainCoinBank();
        productChamber = new ProductChamber();
        productOutDeck =  new ArrayList<>();

        machine = new VendingMachine(
            display,
            userCoinBank,
            machineBank,
            coinOutDeck,
            productChamber,
            productOutDeck
        );
    }

    private void createContext() {
        ctx = new MachineContext(machine);
        ctx.setState(new Idle());
    }

    private void insertProducts() throws WrongProductTypeInsertIntoShelfException {
        int shelfNr = 0;
        int prodId = 0;

        Product product1 = new Product(++prodId, new ProductType.Builder("Cola", BigDecimal.valueOf(8.0)).vendorName("CocaCola").build());
        Product product2 = new Product(++prodId, new ProductType.Builder("Cola Light", BigDecimal.valueOf(21.0)).vendorName("CocaCola").build());
        Product product3 = new Product(++prodId, new ProductType.Builder("Snickers Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());
        Product product4 = new Product(++prodId, new ProductType.Builder("Mars Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());

        machine.addProduct(++shelfNr, product1);
        machine.addProduct(++shelfNr, product2);
        machine.addProduct(++shelfNr, product3);
        machine.addProduct(++shelfNr, product4);
    }

    @Test
    public void insertCoin_whenIdleState_shouldGiveBackCoin(){
        CoinBank expectedCoinOutDeck = new MainCoinBank();
        expectedCoinOutDeck.add(Coin.ONE);

        ctx.inputCoin(Coin.ONE);

        assertEquals(new MainCoinBank().pull(), userCoinBank.pull());
        assertProductChamberIsFull();
        assertEquals(new ArrayList<Product>(), productOutDeck);
        assertEquals(expectedCoinOutDeck.getCoinsQtm(), coinOutDeck.getCoinsQtm());
        assertEquals(expectedCoinOutDeck.getBalance(), coinOutDeck.getBalance());
        assertEquals(expectedCoinOutDeck.pull(), coinOutDeck.pull());

    }

    private void assertProductChamberIsFull() {
        ProductChamberTest.assertProductChamberIsFull(productChamber);
    }


    @Test
    public void pushCancel_whenIdleStateWithoutSelectedProduct_shouldChangeNothing(){

        ctx.pushCancelButton();

        assertEquals(new MainCoinBank().pull(), userCoinBank.pull());
        assertProductChamberIsFull();
        assertEquals(new ArrayList<Product>(), productOutDeck);
        assertEquals(new MainCoinBank().pull(), coinOutDeck.pull());

    }

    @Test
    public void shelfChosen_whenIdleStateAndEmptyProductChamber_shouldGetProductFromChamber() throws WrongProductTypeInsertIntoShelfException {
        Product product2 = new Product(2, new ProductType.Builder("Cola Light", BigDecimal.valueOf(21.0)).vendorName("CocaCola").build());
        Product product3 = new Product(3, new ProductType.Builder("Snickers Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());
        Product product4 = new Product(4, new ProductType.Builder("Mars Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());

        ProductChamber expectedChamber = new ProductChamber();
        expectedChamber.add(2, product2);
        expectedChamber.add(3, product3);
        expectedChamber.add(4, product4);


        ctx.inputShelfNumber(1);

        assertEquals(new MainCoinBank().pull(), userCoinBank.pull());
        assertEquals( expectedChamber.toString(),productChamber.toString());
        assertEquals(new ArrayList<Product>(), productOutDeck);
        assertEquals(new MainCoinBank().pull(), coinOutDeck.pull());

    }


    @Test
    public void addProduct_shouldAddProdToChamber() throws WrongProductTypeInsertIntoShelfException {
        int shelfNr = 0;
        int prodId = 0;

        Product product1 = new Product(++prodId, new ProductType.Builder("Cola", BigDecimal.valueOf(33.3)).vendorName("CocaCola").build());
        Product product2 = new Product(++prodId, new ProductType.Builder("Cola Light", BigDecimal.valueOf(21.0)).vendorName("CocaCola").build());
        Product product3 = new Product(++prodId, new ProductType.Builder("Snickers Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());


        ProductChamber expectedChamber = new ProductChamber();
        expectedChamber.add(++shelfNr, product1);
        expectedChamber.add(++shelfNr, product2);
        expectedChamber.add(++shelfNr, product3);

        insertProducts();

        assertEquals(new MainCoinBank().getCoinsQtm(), userCoinBank.getCoinsQtm());
        assertEquals(new MainCoinBank().getCoinsQtm(), machineBank.getCoinsQtm());
        assertEquals(new MainCoinBank().getCoinsQtm(), coinOutDeck.getCoinsQtm());
        assertEquals(new ArrayList<Product>(), productOutDeck);
        assertProductChamberIsFull();
        assertEquals(expectedChamber.getProductInfoFromShelf(1), productChamber.getProductInfoFromShelf(1));
        assertEquals(expectedChamber.getProductInfoFromShelf(2), productChamber.getProductInfoFromShelf(2));
        assertEquals(expectedChamber.getProductInfoFromShelf(3), productChamber.getProductInfoFromShelf(3));
        assertEquals(2, getProductChamberQuantityOnShelf(1));
        assertEquals(2, getProductChamberQuantityOnShelf(2));
        assertEquals(2, getProductChamberQuantityOnShelf(3));
    }


    @Test
    public void prodChosen_noChange_shouldGiveBackChange() throws WrongProductTypeInsertIntoShelfException {
        int shelfNr = 0;
        int prodId = 0;
        ProductChamber expectedChamber = new ProductChamber();
        Product product1 = new Product(++prodId, new ProductType.Builder("Cola", BigDecimal.valueOf(8.0)).vendorName("CocaCola").build());

        Product product2 = new Product(++prodId, new ProductType.Builder("Cola Light", BigDecimal.valueOf(21.0)).vendorName("CocaCola").build());
        Product product3 = new Product(++prodId, new ProductType.Builder("Snickers Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());
        Product product4 = new Product(++prodId, new ProductType.Builder("Mars Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());

        expectedChamber.add(++shelfNr, product1);
        expectedChamber.add(++shelfNr, product2);
        expectedChamber.add(++shelfNr, product3);
        expectedChamber.add(++shelfNr, product4);

        MainCoinBank expectedCoinOutDeck = new MainCoinBank();
        expectedCoinOutDeck.add(Coin.FIVE);
        expectedCoinOutDeck.add(Coin.FIVE);

        ctx.inputShelfNumber(1);
        ctx.inputCoin(Coin.FIVE);
        ctx.inputCoin(Coin.FIVE);

        assertEquals(new MainCoinBank().pull(), userCoinBank.pull());
        assertProductChamberIsFull();
        assertEquals("On shelf 1 should be back 1 product", 1 , getProductChamberQuantityOnShelf(1));
        assertEquals(expectedChamber.toString(), productChamber.toString());
        assertEquals(new ArrayList<Product>(), productOutDeck);
        assertEquals(expectedCoinOutDeck.pull(), coinOutDeck.pull());

    }

    private int getProductChamberQuantityOnShelf(int i) {
        return ProductChamberTest.getProductChamberQuantityOnShelf(1, productChamber);
    }

    @Test
    public void prodChosen_exactlyAmount_shouldGiveBackProductAndCoinsPutToChamber() throws WrongProductTypeInsertIntoShelfException {
        int shelfNr = 1;
        int prodId = 1;
        ProductChamber expectedChamber = new ProductChamber();
        Product expectedReturnedProduct = new Product(1, new ProductType.Builder("Cola", BigDecimal.valueOf(8.0)).vendorName("CocaCola").build());

        Product product2 = new Product(++prodId, new ProductType.Builder("Cola Light", BigDecimal.valueOf(21.0)).vendorName("CocaCola").build());
        Product product3 = new Product(++prodId, new ProductType.Builder("Snickers Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());
        Product product4 = new Product(++prodId, new ProductType.Builder("Mars Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());

        expectedChamber.add(++shelfNr, product2);
        expectedChamber.add(++shelfNr, product3);
        expectedChamber.add(++shelfNr, product4);

        MainCoinBank expectedCoinBank = new MainCoinBank();
        expectedCoinBank.add(Coin.FIVE);
        expectedCoinBank.add(Coin.ONE);
        expectedCoinBank.add(Coin.TWO);

        List<Product> expectedProductOutDeck = new ArrayList<Product>();
        expectedProductOutDeck.add(expectedReturnedProduct);

        ctx.inputShelfNumber(1);
        ctx.inputCoin(Coin.FIVE);
        ctx.inputCoin(Coin.TWO);
        ctx.inputCoin(Coin.ONE);

        assertEquals(expectedCoinBank.pull(), machineBank.pull());
        assertEquals("On shelf 1 shouldn't be any product", 0 ,getProductChamberQuantityOnShelf(1));
        assertEquals(expectedChamber.toString(), productChamber.toString());
        assertEquals(expectedProductOutDeck, productOutDeck);
        assertEquals(new MainCoinBank().pull(), coinOutDeck.pull());

    }



}
