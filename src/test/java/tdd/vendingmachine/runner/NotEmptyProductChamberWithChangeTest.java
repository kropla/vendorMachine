package tdd.vendingmachine.runner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.context.MachineContext;
import tdd.vendingmachine.context.state.Idle;
import tdd.vendingmachine.display.Display;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.money.Coin;
import tdd.vendingmachine.money.CoinBank;
import tdd.vendingmachine.money.MainCoinBank;
import tdd.vendingmachine.product.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * Created by kropla on 26.01.17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NotEmptyProductChamberWithChangeTest {
    private VendingMachine machine;
    private Context ctx;
    private CoinBank userCoinBank = new MainCoinBank();
    private CoinBank machineBank = new MainCoinBank();
    private CoinBank coinOutDeck = new MainCoinBank();
    private ProductChamber productChamber = new ProductChamber();
    private List<Product> productOutDeck;


    @Before
    public void initMachine() throws WrongProductTypeInsertIntoShelfException {
        createMachine();
        createContext();
        insertProducts();
    }



    private void createMachine() {
        Display display  = mock(Display.class);
        userCoinBank = new MainCoinBank();
        machineBank = new MainCoinBank();
        coinOutDeck = new MainCoinBank();
        productChamber = new ProductChamber();
        productOutDeck =  new ArrayList<>();

        insertCoinsToMainBank();

        machine = new VendingMachine(
            display,
            userCoinBank,
            machineBank,
            coinOutDeck,
            productChamber,
            productOutDeck
        );
    }

    private void insertCoinsToMainBank() {
        Map<Coin, Integer> coins = new EnumMap<>(Coin.class);
        coins.put(Coin.FIVE,6);
        coins.put(Coin.TWO,6);
        coins.put(Coin.ONE,6);
        coins.put(Coin.ONE_HALF,6);
        coins.put(Coin.TWO_TENTHS,6);
        coins.put(Coin.ONE_TENTH,6);

        machineBank.add(coins);
    }

    private void createContext() {
        ctx = new MachineContext(machine);
        ctx.setState(new Idle());
    }

    private void insertProducts() throws WrongProductTypeInsertIntoShelfException {
        int shelfNr = 0;
        int prodId = 0;

        Product product1 = new Product(++prodId, new ProductType.Builder("Cola", BigDecimal.valueOf(8.0)).vendorName("CocaCola").build());
        Product product2 = new Product(++prodId, new ProductType.Builder("Cola Light", BigDecimal.valueOf(21.0)).vendorName("CocaCola").build());
        Product product3 = new Product(++prodId, new ProductType.Builder("Snickers Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());
        Product product4 = new Product(++prodId, new ProductType.Builder("Mars Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());

        machine.addProduct(++shelfNr, product1);
        machine.addProduct(++shelfNr, product2);
        machine.addProduct(++shelfNr, product3);
        machine.addProduct(++shelfNr, product4);
    }


    @Test
    public void prodChosen_moreCoinsAmount_shouldGiveBackProductAndChange() throws WrongProductTypeInsertIntoShelfException {
        int shelfNr = 1;
        int prodId = 1;
        ProductChamber expectedChamber = new ProductChamber();
        Product expectedReturnedProduct = new Product(1, new ProductType.Builder("Cola", BigDecimal.valueOf(8.0)).vendorName("CocaCola").build());

        Product product2 = new Product(++prodId, new ProductType.Builder("Cola Light", BigDecimal.valueOf(21.0)).vendorName("CocaCola").build());
        Product product3 = new Product(++prodId, new ProductType.Builder("Snickers Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());
        Product product4 = new Product(++prodId, new ProductType.Builder("Mars Bar", BigDecimal.valueOf(12.2)).vendorName("Mars").build());

        expectedChamber.add(++shelfNr, product2);
        expectedChamber.add(++shelfNr, product3);
        expectedChamber.add(++shelfNr, product4);

        Map<Coin, Integer> expectedCoins = new EnumMap<>(Coin.class);
        expectedCoins.put(Coin.FIVE,8);
        expectedCoins.put(Coin.TWO,5);
        expectedCoins.put(Coin.ONE,6);
        expectedCoins.put(Coin.ONE_HALF,6);
        expectedCoins.put(Coin.TWO_TENTHS,6);
        expectedCoins.put(Coin.ONE_TENTH,6);

        MainCoinBank expectedMachineBank = new MainCoinBank();
        expectedMachineBank.add(expectedCoins);

        MainCoinBank change = new MainCoinBank();
        change.add(Coin.TWO);
        change.add(Coin.TWO);

        List<Product> expectedProductOutDeck = new ArrayList<Product>();
        expectedProductOutDeck.add(expectedReturnedProduct);

        ctx.inputShelfNumber(1);
        ctx.inputCoin(Coin.FIVE);
        ctx.inputCoin(Coin.TWO);
        ctx.inputCoin(Coin.FIVE);

        assertEquals("On shelf 2 should be one product", 1 , ProductChamberTest.getProductChamberQuantityOnShelf(2, productChamber));
        assertEquals("On shelf 1 shouldn't be any product", 0 , ProductChamberTest.getProductChamberQuantityOnShelf(1, productChamber));
        assertEquals(expectedChamber.toString(), productChamber.toString());
        assertEquals(expectedProductOutDeck, productOutDeck);
        assertEquals(change.pull(), coinOutDeck.pull());
        assertEquals(expectedMachineBank.pull(), machineBank.pull());

    }



}
