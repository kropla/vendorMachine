package tdd.vendingmachine.money;

import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.Map;

/**
 * Created by kropla on 17.01.17.
 */
public class SimpleCoinChanger implements CoinChanger {

    @Override
    public Map<Coin, Integer> change(BigDecimal price, BigDecimal insertedCoinsBalance, CoinBank cBank) throws NotSufficientCoinsInBankForChange {
        Map<Coin, Integer> change = new EnumMap<>(Coin.class);
        int diff = getDifferenceInIntWhitoutComma(insertedCoinsBalance, price);

        for(Coin c : Coin.values()){

            if (diff >= c.getIntChangeValue() ){
                int neededCoinTypeQtm = diff/((int)(c.getIntChangeValue()));
                neededCoinTypeQtm = cBank.pull(c, neededCoinTypeQtm).get(c);
                if (neededCoinTypeQtm > 0){
                    change.put(c, neededCoinTypeQtm);
                    diff -= (neededCoinTypeQtm * c.getIntChangeValue());
                }
            }
        }

        if (diff > 0){
            cBank.add(change);
            throw new NotSufficientCoinsInBankForChange();
        }

        return change;
    }

    int getDifferenceInIntWhitoutComma(BigDecimal insertedCoinsBalance, BigDecimal price) {
        BigDecimal priceRounded = price.setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal insertedCoinsBalanceRounded = insertedCoinsBalance.setScale(2,BigDecimal.ROUND_HALF_UP);
        BigDecimal diff = insertedCoinsBalanceRounded.subtract(priceRounded);
        int result = diff.multiply(BigDecimal.TEN).intValue();

        return result;
    }
}
