package tdd.vendingmachine.money;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by kropla on 15.01.17.
 */
public interface CoinBank {
    void add(Coin coin);

    int getCoinsQtm();

    BigDecimal getBalance();

    void add(Map<Coin, Integer> coins);

    Map<Coin,Integer> pull();

    Map<Coin,Integer> pull(Coin coin);

    Map<Coin,Integer> pull(Coin coin, int qtm);
}
