package tdd.vendingmachine.money;

import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.Map;

/**
 * Created by kropla on 15.01.17.
 */
public class MainCoinBank implements CoinBank {
    Map<Coin, Integer> coinsBank = new EnumMap<>(Coin.class);

    @Override
    public void add(Coin coin) {
        Integer qtm = 1;
        if(coinsBank.containsKey(coin)){
            qtm = coinsBank.get(coin);
            qtm ++;
        }
        coinsBank.put(coin, qtm);
    }

    @Override
    public int getCoinsQtm() {
        return coinsBank.values().stream().mapToInt(i -> i.intValue()).sum();
    }

    @Override
    public BigDecimal getBalance() {
        BigDecimal balance = BigDecimal.valueOf(0.0);
        for (Map.Entry<Coin,Integer> entry : coinsBank.entrySet()){
            balance = balance.add(BigDecimal.valueOf(entry.getKey().getValue() * entry.getValue()));
        }
        return balance;
    }

    @Override
    public void add(Map<Coin, Integer> coins) {
        if (coinsBank.isEmpty()) {
            coinsBank.putAll(coins);
        } else {
            for(Map.Entry<Coin, Integer> entry : coins.entrySet()){
                if (coinsBank.containsKey(entry.getKey())){
                    int qtm = coinsBank.get(entry.getKey()) + entry.getValue();
                    coinsBank.put(entry.getKey(), qtm);
                } else {
                    coinsBank.put(entry.getKey(), entry.getValue());
                }
            }
        }
    }

    @Override
    public Map<Coin, Integer> pull() {
        Map<Coin, Integer> coinsToReturn = coinsBank;
        coinsBank = new EnumMap<>(Coin.class);
        return coinsToReturn;
    }

    @Override
    public Map<Coin, Integer> pull(Coin coin) {
        Map<Coin, Integer> mapToReturn = new EnumMap<>(Coin.class);
        if (coinsBank.containsKey(coin)) {
            mapToReturn.put(coin, coinsBank.get(coin));
            coinsBank.remove(coin);
        } else {
            mapToReturn.put(coin, 0);
        }
        return mapToReturn;
    }

    @Override
    public Map<Coin, Integer> pull(Coin coin, int qtm) {
        Map<Coin, Integer> mapToReturn = new EnumMap<>(Coin.class);
        if (coinsBank.containsKey(coin)) {

            int coinsQtmRemainInBank = coinsBank.get(coin) - qtm;
            int realCoinQtmToReturn = qtm;
            if (coinsQtmRemainInBank <= 0){
                realCoinQtmToReturn = coinsBank.get(coin);
                coinsBank.remove(coin);
            } else {
                coinsBank.put(coin, coinsQtmRemainInBank);
            }
            mapToReturn.put(coin, realCoinQtmToReturn);

        } else {
            mapToReturn.put(coin, 0);
        }
        return mapToReturn;
    }

    @Override
    public String toString(){
        return coinsBank.toString();
    }
}
