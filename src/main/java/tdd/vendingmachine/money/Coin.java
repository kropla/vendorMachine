package tdd.vendingmachine.money;

/**
 * Created by kropla on 15.01.17.
 */
public enum Coin {
    FIVE(5.0, 50),
    TWO(2.0, 20),
    ONE(1.0, 10),
    ONE_HALF(0.5, 5),
    TWO_TENTHS(0.2, 2),
    ONE_TENTH(0.1, 1);

    private double value;
    private double intChangeValue;

    Coin(double value, int changeValue) {
        this.value = value;
        this.intChangeValue = changeValue;
    }

    public double getValue() {
        return value;
    }

    public double getIntChangeValue() {
        return intChangeValue;
    }
}

