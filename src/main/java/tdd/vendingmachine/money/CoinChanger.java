package tdd.vendingmachine.money;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by kropla on 17.01.17.
 */
@FunctionalInterface
public interface CoinChanger {
    Map<Coin,Integer> change(BigDecimal price, BigDecimal insertedCoinsBalance, CoinBank cBank) throws NotSufficientCoinsInBankForChange;

}
