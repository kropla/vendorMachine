package tdd.vendingmachine.money;

/**
 * Created by Maciej_Kroplewski on 1/18/2017.
 */
public class NotSufficientCoinsInBankForChange extends Exception {
    public NotSufficientCoinsInBankForChange() {
        super ("There is no sufficient coins in bank for change.");
    }
}
