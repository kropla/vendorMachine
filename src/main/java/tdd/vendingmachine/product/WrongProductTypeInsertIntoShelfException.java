package tdd.vendingmachine.product;

/**
 * Created by kropla on 14.01.17.
 */
public class WrongProductTypeInsertIntoShelfException extends Exception {
    public WrongProductTypeInsertIntoShelfException() {
        super("On the single shelf there must be insterted products of the same types");
    }
}
