package tdd.vendingmachine.product;

/**
 * Created by kropla on 14.01.17.
 */
public class Product {
    private int id;
    private ProductType type;

    public Product(int id, ProductType prodType) {
        this.id = id;
        this.type = prodType;
    }

    public ProductType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()){
            return false;
        }

        Product product = (Product) o;

        if (id != product.id) {
            return false;
        }

        return type.equals(product.type);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + type.hashCode();
        return result;
    }
}
