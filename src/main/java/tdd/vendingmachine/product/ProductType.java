package tdd.vendingmachine.product;

import java.math.BigDecimal;

/**
 * Created by kropla on 14.01.17.
 * Changed from enum to normal class because type should be editable and possible to create new one
 */
public class ProductType {
    private String name;
    private BigDecimal price;
    private String vendorName;
    private int weightInGrams;
    private float capacityInLiters;
    private int heightInMilimeters;
    private int widthInMilimeters;

    public ProductType(Builder builder) {
        name = builder.name;
        price = builder.price;
        vendorName = builder.vendorName;
        weightInGrams = builder.weightInGrams;
        capacityInLiters = builder.capacityInLiters;
        heightInMilimeters = builder.heightInMilimeters;
        widthInMilimeters = builder.widthInMilimeters;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public static class Builder {
        private String name;
        private BigDecimal price;
        private String vendorName;
        private int weightInGrams;
        private float capacityInLiters;
        private int heightInMilimeters;
        private int widthInMilimeters;

        public Builder(String prodTypeName, BigDecimal prodPrice) {
            name = prodTypeName;
            price = prodPrice;
        }

        public Builder(String prodTypeName, double prodPrice) {
            name = prodTypeName;
            price = BigDecimal.valueOf(prodPrice);
        }


        public Builder vendorName(String vname) {
            vendorName = vname;
            return this;
        }

        public Builder weightInGrams(int weight) {
            this.weightInGrams = weight;
            return this;
        }

        public Builder capacityInLiters(float capacity) {
            this.capacityInLiters = capacity;
            return this;
        }

        public Builder heightInMilimeters(int heigh) {
            this.heightInMilimeters = heigh;
            return this;
        }

        public Builder widthInMilimeters(int width) {
            this.widthInMilimeters = width;
            return this;
        }

        public ProductType build() { return new ProductType(this); }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductType)) {
            return false;
        }

        ProductType that = (ProductType) o;

        if (weightInGrams != that.weightInGrams) {
            return false;
        }
        if (heightInMilimeters != that.heightInMilimeters) {
            return false;
        }
        if (widthInMilimeters != that.widthInMilimeters) {
            return false;
        }
        if (!name.equals(that.name)) {
            return false;
        }
        return vendorName.equals(that.vendorName);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (vendorName != null ? vendorName.hashCode() : 0);
        result = 31 * result + weightInGrams;
        result = 31 * result + heightInMilimeters;
        result = 31 * result + widthInMilimeters;
        return result;
    }
}
