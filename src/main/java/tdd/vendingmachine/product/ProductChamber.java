package tdd.vendingmachine.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kropla on 14.01.17.
 */
public class ProductChamber {
    private Map<Integer, List<Product>> shelves = new HashMap<>();

    public void add(Integer shelfNumber, Product productToAdd) throws WrongProductTypeInsertIntoShelfException{
        List<Product> productsFromShelf = getProductsFromShelf(shelfNumber);

        if (shelfIsEmpty(productsFromShelf)){
            productsFromShelf = new ArrayList<>();
            shelves.put(shelfNumber,productsFromShelf);
        } else {
            Product productFromShelf = productsFromShelf.get(0);
            checkProductTypesCompability(productToAdd, productFromShelf);
        }
        productsFromShelf.add(productToAdd);

    }

    private boolean shelfIsEmpty(List<Product> products) {
        return products == null;
    }

    private List<Product> getProductsFromShelf(Integer shelfNumber) {
        return shelves.get(shelfNumber);
    }

    private void checkProductTypesCompability(Product productToAdd, Product productFromShelf) throws WrongProductTypeInsertIntoShelfException{
        if (!productToAdd.getType().equals(productFromShelf.getType())){
            throw new WrongProductTypeInsertIntoShelfException();
        }
    }

    public Product getProductInfoFromShelf(Integer shelfNumber) {
        return getProductFromShelf(shelfNumber);
    }

    public void removeOneProductFromShelf(int shelfNumber) {
        List<Product> products = getProductsFromShelf(shelfNumber);
        if(!shelfIsEmpty(products)){
            products.remove(0);
            if (products.isEmpty()){
                shelves.remove(shelfNumber);
            }
        }

    }

    private Product getProductFromShelf(Integer shelfNumber) {
        Product productInfo = null;
        List<Product> products = getProductsFromShelf(shelfNumber);
        if (!shelfIsEmpty(products)){
            productInfo = products.get(0);
        }
        return productInfo;
    }

    @Override
    public String toString(){
        return shelves.toString();
    }
}
