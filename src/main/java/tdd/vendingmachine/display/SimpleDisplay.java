package tdd.vendingmachine.display;

import java.util.ResourceBundle;

/**
 * Created by kropla on 14.01.17.
 */
public class SimpleDisplay implements Display {

    private ResourceBundle resourceBundle;
    private Displayer displayer;

    public SimpleDisplay(ResourceBundle resBundle, Displayer argDisplayer) {
        this.resourceBundle = resBundle;
        this.displayer = argDisplayer;
    }

    @Override
    public void showProductPrice(Double priceIn) {
        produceMessage(priceIn);
    }

    @Override
    public void showMessage(Message msg) {
        produceMessage(msg.getPropertyName());
    }

    private void produceMessage(String msgSymbol) {
        displayer.show(resourceBundle.getString(msgSymbol));
    }
    private void produceMessage(Double priceIn) {
        displayer.show(priceIn.toString());
    }

}
