package tdd.vendingmachine.display;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by kropla on 21.01.17.
 */
public class LoggerDisplayer implements Displayer {
    private static final Logger LOG = LoggerFactory.getLogger(LoggerDisplayer.class);
    @Override
    public void show(String string) {
        LOG.info(string);
    }
}
