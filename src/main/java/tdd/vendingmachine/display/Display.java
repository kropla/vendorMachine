package tdd.vendingmachine.display;

/**
 * Created by kropla on 14.01.17.
 */
public interface Display {

    void showProductPrice(Double priceIn);

    void showMessage(Message msg);
}
