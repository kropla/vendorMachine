package tdd.vendingmachine.display;

/**
 * Created by kropla on 21.01.17.
 */
public class SystemOutDisplayer implements Displayer {
    @Override
    public void show(String string) {
        System.out.println(string);
    }
}
