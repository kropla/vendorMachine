package tdd.vendingmachine.display;

/**
 * Created by kropla on 21.01.17.
 */
public enum Message {
    MSG_WELCOME ("msg.info.welcome"),
    MSG_CHANGE_WARNING ("msg.warning.change"),
    MSG_WRONG_SHELF ("msg.warning.wrongShelf"),
    MSG_CANCEL ("msg.info.cancel");

    private String propertyName;

    Message(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }
}
