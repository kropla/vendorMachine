package tdd.vendingmachine.display;

/**
 * Created by kropla on 21.01.17.
 */
public interface Displayer {

    void show(String string);
}
