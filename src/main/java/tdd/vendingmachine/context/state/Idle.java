package tdd.vendingmachine.context.state;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.display.Message;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.money.Coin;
/**
 * Created by kropla on 18.01.17.
 */
public class Idle implements MachineState {
    private static final Logger LOG = LoggerFactory.getLogger(Idle.class);

    @Override
    public void pushCancelButton(VendingMachine machine, Context context) {
        LOG.info("Cancel button has been pushed. Do nothing. Just repeat welcome msg on display");
        machine.displayMessage(Message.MSG_WELCOME);
    }

    @Override
    public void initState(VendingMachine machine, Context context) {
        LOG.info("Initiating Idle state. Just welcome msg on display");
        machine.displayMessage(Message.MSG_WELCOME);
    }

    @Override
    public void inputCoin(VendingMachine machine, Context context, Coin coinToInput) {
        LOG.info("Initiating Idle state. Coin was inserted. Need to giveBack the same coin");
        machine.giveBackUserCoin(coinToInput);
        machine.displayMessage(Message.MSG_WELCOME);
    }

    @Override
    public void inputShelfNumber(VendingMachine machine, Context context, int shelfNr) {
        LOG.info("User has chosen shelf number: {}", shelfNr);
        machine.createOrder(shelfNr);
        if (machine.getChosenProduct() != null){
            context.setState(new CoinWaiting());
            context.initState();
        } else {
            machine.displayMessage(Message.MSG_WRONG_SHELF);
        }
    }
}
