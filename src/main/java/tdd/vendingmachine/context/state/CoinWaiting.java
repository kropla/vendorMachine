package tdd.vendingmachine.context.state;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.money.Coin;

/**
 * Created by Maciej_Kroplewski on 1/20/2017.
 */
public class CoinWaiting implements MachineState{
    private static final Logger LOG = LoggerFactory.getLogger(CoinWaiting.class);

    @Override
    public void pushCancelButton(VendingMachine machine, Context context) {
        LOG.info("User canceled order.");
        machine.returnProductToChamber();
        machine.giveBackUserCoins();
        context.setState(new Idle());
        context.initState();
    }

    @Override
    public void initState(VendingMachine machine, Context context) {
        LOG.info("Waiting for coins. Displaying product price.");
        machine.displayRemaingCoinAmount();
    }

    @Override
    public void inputCoin(VendingMachine machine, Context context, Coin coinToInput) {
        LOG.info("User inputted coin {}", coinToInput);
        machine.inputCoin(coinToInput);
        if (machine.isUserCoinAmountEnough()){
            context.setState(new CoinChanging());
            context.initState();
        } else {
            machine.displayRemaingCoinAmount();
        }
    }

    @Override
    public void inputShelfNumber(VendingMachine machine, Context context, int shelfNr) {
        LOG.info("User inputted shelf nr. Machine is in waiting for coin state. Do nothing");
        machine.displayRemaingCoinAmount();
    }
}
