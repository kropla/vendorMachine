package tdd.vendingmachine.context.state;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.money.*;

/**
 * Created by kropla on 21.01.17.
 */
public class CoinChanging implements MachineState {
    private static final Logger LOG = LoggerFactory.getLogger(CoinChanging.class);

    @Override
    public void initState(VendingMachine machine, Context context) {
        LOG.info("Init state for CoinChangingState.");
        //get all users coins
        try {
            machine.changeCoins();
            context.setState(new ProductIssuing());
        } catch (NotSufficientCoinsInBankForChange ex){
            LOG.info("Can't change. There is not enough coins in bank", ex);
            machine.giveBackUserCoins();
            machine.returnProductToChamber();
            context.setState(new Idle());
        }
        context.initState();
    }

    @Override
    public void inputCoin(VendingMachine machine, Context context, Coin coinToInput) {
        LOG.info("User inputted coin. Give back coins.");
        machine.giveBackUserCoin(coinToInput);
    }

    @Override
    public void pushCancelButton(VendingMachine machine, Context context) {
        LOG.info("Cancel button was pushed. Do nothing.");
    }

    @Override
    public void inputShelfNumber(VendingMachine machine, Context context, int shelfNr) {
        LOG.info("Cancel button was pushed. Do nothing.");
    }
}
