package tdd.vendingmachine.context.state;

import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.money.Coin;

/**
 * Created by kropla on 18.01.17.
 */
public interface MachineState {
    void pushCancelButton(VendingMachine machine, Context context);

    void initState(VendingMachine machine, Context context);

    void inputCoin(VendingMachine machine, Context context, Coin coinToInput);

    void inputShelfNumber(VendingMachine machine, Context context, int shelfNr);
}
