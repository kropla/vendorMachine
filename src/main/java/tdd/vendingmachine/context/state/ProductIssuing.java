package tdd.vendingmachine.context.state;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdd.vendingmachine.context.Context;
import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.money.Coin;

/**
 * Created by kropla on 22.01.17.
 */
public class ProductIssuing implements MachineState {
    private static final Logger LOG = LoggerFactory.getLogger(ProductIssuing.class);

    @Override
    public void initState(VendingMachine machine, Context context) {
        LOG.info("Product will be issued.");
        machine.putProductToDock();
        context.setState(new Idle());
        context.initState();
    }

    @Override
    public void inputCoin(VendingMachine machine, Context context, Coin coinToInput) {
        LOG.info("User inputted coin. Give back coins.");
        machine.giveBackUserCoin(coinToInput);
    }

    @Override
    public void inputShelfNumber(VendingMachine machine, Context context, int shelfNr) {
        LOG.info("Cancel button was pushed. Do nothing.");
    }

    @Override
    public void pushCancelButton(VendingMachine machine, Context context) {
        LOG.info("Cancel button was pushed. Do nothing.");
    }
}
