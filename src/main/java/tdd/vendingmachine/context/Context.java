package tdd.vendingmachine.context;

import tdd.vendingmachine.money.Coin;
import tdd.vendingmachine.context.state.MachineState;

/**
 * Created by Maciej_Kroplewski on 1/23/2017.
 */
public interface Context {

void setState(MachineState state);

void inputCoin(Coin coin);

void pushCancelButton();

void initState();

void inputShelfNumber(int shelfNr);
}
