package tdd.vendingmachine.context;

import tdd.vendingmachine.machine.VendingMachine;
import tdd.vendingmachine.money.Coin;
import tdd.vendingmachine.context.state.MachineState;

/**
 * Created by Maciej_Kroplewski on 1/23/2017.
 */
public class MachineContext implements Context {
    private MachineState state;
    private VendingMachine machine;

    public MachineContext(VendingMachine machine) {
        this.machine = machine;
    }

    @Override
    public void setState(MachineState state) {
        this.state = state;
    }

    @Override
    public void inputCoin(Coin coin){
        state.inputCoin(machine, this, coin);
    }

    @Override
    public void pushCancelButton(){
        state.pushCancelButton(machine,this);
    }

    @Override
    public void initState(){
        state.initState(machine, this);
    }

    @Override
    public void inputShelfNumber(int shelfNr){
        state.inputShelfNumber(machine, this, shelfNr);
    }

    public MachineState getState() {
        return state;
    }
}
