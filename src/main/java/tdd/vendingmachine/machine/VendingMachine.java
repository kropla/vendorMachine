package tdd.vendingmachine.machine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdd.vendingmachine.display.Display;
import tdd.vendingmachine.display.Message;
import tdd.vendingmachine.money.*;
import tdd.vendingmachine.product.Product;
import tdd.vendingmachine.product.ProductChamber;
import tdd.vendingmachine.product.WrongProductTypeInsertIntoShelfException;
import tdd.vendingmachine.product.ProductType;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class VendingMachine {
    private static final Logger LOG = LoggerFactory.getLogger(VendingMachine.class);

    private Display display;
    private CoinBank userInputCoinsBank;
    private CoinBank machineMainCoinsBank;
    private CoinBank coinDockOutShelf;
    private ProductChamber productChamber;
    private List<Product> productOutDeck;
    private Integer chosenShelfNr;
    private Product chosenProduct;

    public VendingMachine(Display display, CoinBank userInputCoinsBank, CoinBank machineMainCoinsBank, CoinBank coinDockOutShelf, ProductChamber productChamber) {
        this.display = display;
        this.userInputCoinsBank = userInputCoinsBank;
        this.machineMainCoinsBank = machineMainCoinsBank;
        this.coinDockOutShelf = coinDockOutShelf;
        this.productChamber = productChamber;
    }

    public VendingMachine(Display display, CoinBank userInputCoinsBank, CoinBank machineMainCoinsBank, CoinBank coinDockOutShelf, ProductChamber productChamber, List productOutDeck) {
        this(display, userInputCoinsBank, machineMainCoinsBank, coinDockOutShelf, productChamber);
        this.productOutDeck = productOutDeck;
    }

    public void displayMessage(Message msg) {
        display.showMessage(msg);
    }

    public void displayRemaingCoinAmount() {
        ProductType prodType = getChosenProduct().getType();

        BigDecimal userCoinsAmount = userInputCoinsBank.getBalance();
        BigDecimal remainAmount = prodType.getPrice().subtract(userCoinsAmount);
        display.showProductPrice(remainAmount.doubleValue());
    }

    public void inputCoin(Coin coinToInput) {
        userInputCoinsBank.add(coinToInput);
    }

    public void giveBackUserCoins() {
        Map<Coin, Integer> coins = userInputCoinsBank.pull();
        coinDockOutShelf.add(coins);
    }

    public void giveBackUserCoin(Coin coinToInput) {
        coinDockOutShelf.add(coinToInput);
    }

    public boolean isUserCoinAmountEnough() {
        ProductType prodType = getChosenProduct().getType();
        boolean equalOrBigger = false;
        if(userInputCoinsBank.getBalance().compareTo(prodType.getPrice()) >=0 ) {
            equalOrBigger = true;
        }
        return equalOrBigger;
    }

    public void createOrder(int shelfNr) {
        this.chosenProduct = productChamber.getProductInfoFromShelf(shelfNr);
        if (this.chosenProduct != null){
            this.chosenShelfNr = shelfNr;
            productChamber.removeOneProductFromShelf(shelfNr);
        }
    }

    public Product getChosenProduct() {
        return chosenProduct;
    }

    private void clearOrder() {
        this.chosenShelfNr = null;
        chosenProduct = null;
    }

    public void returnProductToChamber() {
        try {
            this.productChamber.add(getChosenShelfNr(), getChosenProduct());
            clearOrder();
        } catch (WrongProductTypeInsertIntoShelfException e) {
            LOG.error("", e);
        }
    }

    public void addProduct(int shelfNr, Product product) throws WrongProductTypeInsertIntoShelfException {
        this.productChamber.add(shelfNr, product);
    }

    public void putProductToDock() {
        productOutDeck.add(getChosenProduct());
        clearOrder();
    }

    public void changeCoins() throws NotSufficientCoinsInBankForChange {
        BigDecimal price = getChosenProduct().getType().getPrice();
        if(userInputCoinsBank.getBalance() != price) {
            CoinChanger changer = new SimpleCoinChanger();
            coinDockOutShelf.add(changer.change(price,
                userInputCoinsBank.getBalance(),
                machineMainCoinsBank));
        }
        machineMainCoinsBank.add(userInputCoinsBank.pull());
    }

    //test
    public Integer getChosenShelfNr() {
        return chosenShelfNr;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("\n\n======================\n");
        sb.append("Vendoring machine stock:\n");
        sb.append("\tProduct chamber:\n\t\t");
        sb.append(productChamber.toString());

        sb.append("\n\tMain machine coin Bank:\n\t\t");
        sb.append(machineMainCoinsBank.toString());

        sb.append("\n\tUser's inputted coins:\n\t\t");
        sb.append(userInputCoinsBank.toString());

        sb.append("\n\tCoin's out deck:\n\t\t");
        sb.append(coinDockOutShelf.toString());

        sb.append("\n\tProducts's out deck:\n\t\t");
        sb.append(productOutDeck.toString());
        sb.append("\n--------------------\n");
        return sb.toString();
    }
}
